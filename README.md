// we have outgrown the preliminary project architecture and will move towards a separation of concerns and projects, parceling out **individual repositories** to streamline **development** and **deployment**. this original repository will serve as a meta-repository, an information hub and for strategy development. repository scheme mirrors previously existing folder structure with some systematic naming conventions to help make it extendable in future projects.

// WiKi for documentation will be maintained here and linked to from all sub projects. Sub projects are delineated by underscore PROJECT_SUB-PROJECT

---

# digital infrastructure (diginfra): wolke-foundation

a collaborative Open Innovation initiative to set up a resilient architecture for self hosted cloud computing and web site deployment. low maintenance, modular, extendable civic infrastructure for hosting projects with Kreatives Leipzig e.V.

we structure design discussions and artifacts with this here digital tool for version control and project management: a repository.

work done in this repository and its governance structure basically functions like an open ended hackathon. the NGO Kreatives Leipzig e.V. (short: KLeV) who owns the repository will provide goals and guidelines through the democratic input of its members. contribution and pull requests welcome, but offering to work for free is not expected: KLeV will pursue funding, including work on upstream fixes.

(btw, if you are a creative in the region, sign up to join! https://kreatives-leipzig.de)


---

### repositories

issues, development and discussions pertaining to work on *digital infrastructure* is structured in

- diginfra: <- strategy & documentation for digital infrastructure projects of KLeV (renamed from wolke-foundation)

- [diginfra_wolke-dev](https://codeberg.org/KLeV/diginfra_wolke-dev): development setup for nextcloud + publishing server + themes in docker container
  - (submodule) wolke-pubserv (main branch, see below)
  - (submodule) wolke-themes (main branch, see below)
- [diginfra_wolke-setup](https://codeberg.org/KLeV/diginfra_wolke-setup): documentation of nextcloud install process and config files
  - (submodule) wolke-pubserv (release-branch, see below)
  - (submodule) wolke-themes (release-branch, see below)
- [diginfra_wolke-pubserv](https://codeberg.org/KLeV/diginfra_wolke-pubserv): (branches: main, release) web app for pushing dynamic web pages from nextcloud staging to respective TLDs via FTP as static pages
- [diginfra_wolke-themes](https://codeberg.org/KLeV/diginfra_wolke-themes): (branches: main, release) picoCMS theme files for web projects published via nextcloud. themes include *klev-home*, *klev-kompass*, *nachtrat*

- [diginfra_events](https://codeberg.org/KLeV/diginfra_events): config files, JS/CSS-files, documentation for setting up a modified Mobilizon instance
- [diginfra_access](https://codeberg.org/KLeV/diginfra_access): static files + potentially API for a physical asset management and lending service, providing access to a library for gadgets and tools

issues, development and discussions pertaining to work on cultural equity and *physical infrastructure* is structured in

- [culturequity](https://codeberg.org/KLeV/culturequity): strategy & documentation for physical infrastructure projects of KLeV
- [culturequity_mediabox](https://codeberg.org/KLeV/culturequity) development & documentation of a studio-in-a-box video production (streaming) solution to make events more accessible
- TBD (funding acquired, projects forthcoming)

issues, development and discussions pertaining to work on *content* is structured in

- [content](https://codeberg.org/KLeV/content): public documents of the NGO, snapshots and backups of content files used in the CMS and/or elsewhere in KLeV web projects

---

## goals

the software architecture 

```
+-> WOLKE
|   nextcloud installation & migration of data to new server
|   + mind: prep for non-destructive markdown use, OpenOffice, Talk (Video)
|   |  
|   +-> STAGING SERVER
|   |   picoCMS on nextcloud: converts flat files & THEMES into various WEBSITES
|   |   + mind: needs to output different themes for different folders
|   |   |
|   |   +-> WEBSITE (RE)DESIGN: KREATIVES-LEIPZIG.DE
|   |   |   a (very) modest style update and reduction of functions to necessities
|   |   |   + mind: no tracking, accessible
|   |   |   + mind: ideally created via new standard-THEME (base for future projects)
|   |   |
|   |   +-> WEBSITE DESIGN: NACHTRAT
|   |   |   style and content architecture via input from Nachtrat (w kickoff)
|   |   |   + mind: no tracking, accessible
|   |   |   + mind: styling, hence THEME distinct from standard-theme
|   |   |
|   |   +-> WEBSITE DESIGN: KOMPETENZ-KOMPASS
|   |   |   style and content architecture for wiki + interactive rummaging-search
|   |   |   + mind: no tracking, accessible
|   |   |   + mind: THEME ideally derivative of standard-theme
|   |   |   + mind: needs JS-hook into API to dynamically load profiles from K-I-S
|   |   |
|   |   +-> (TBD): website design for future sub-projects and responsibilities
|   |       + mind: no tracking, accessible, use standard THEME
|   |
|   +-> PUBLISHING SERVER
|       crawls site output from picoCMS and deploys as static sites to various TLDs
|       + mind: ideally self sufficient, can run on any hosting setup
|
+-> SOCIAL NETWORK
    a branded test instance of mobilizon.org for invite-only use of event management
    + mind: themeing and hosting distinct from other infrastructure
+
|
+-> (TBD) DISCUSSION FORUM: cultural equity
    a low stakes, low barrier-to-entry way of communication. possibly a self-hosted email listing or newsgroup
|   
+-> (TBD) ASSET MANAGEMENT LIBRARY
   + mind: ticketing functionality optional (e.g. consumable QR via microservice)
```

---

## tenets

### 1 we want to pursue a collaborative Open Innovation project

everybody eats, everybody contributes according to their abilities and availability. Work is defined and distributed collaboratively. Remuneration is standardized for all per the previously agreed daily. Work load is estimated in **dailys** for the work items we identify. 

KLeV is drawing from different purses to make the budget fit the work. KLeV will assume fiduciary duty: Admin will notify the team and intervene, if the planned work load looks to go over budget. Admin will also help with planning. Currently the budget is quite flexible and ample to meet the goal. Details cannot be shared publicly, but are discussed with all participants.


### 2 we want to gather all information to facilitate collaboration here in this repository

a **project wiki** in this repository contains the relevant information to execute: a  **road map** outlines the goals and the architecture chosen to meet the goal. **work areas** and sketches of actionable items, as well as **documentation** are being appended in separate entries. Discussions about the entries can take place in  **issues** tagged **meta**.

**work items** are tracked as **issues**. ideally a work item should be defined as a modular component of the road map with a work load that can be guesstimated in days. half a day is fine, too, just make it obvious with Admin so that budgeting is transparent and easily manageable. Bundle smaller action steps into meaningful chunks to form an action item.


### 3 processes are there to serve the people

if the collaborative structure fails us, we find ways to make it work, rather than make people work to meet arbitrary rules.
